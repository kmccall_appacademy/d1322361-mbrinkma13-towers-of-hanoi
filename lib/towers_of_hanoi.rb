# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
  def initialize
    @towers = [[3, 2, 1], [], []]
  end

  attr_reader :towers

  def play
    # loop until all disks have been moved to another pile
    until won?
      puts 'move from pile:'
      from_pile = gets.to_i

      puts 'to pile:'
      to_pile = gets.to_i

      if valid_move?(from_pile, to_pile)
        self.move(from_pile, to_pile)
        self.display
      end
    end
  end

  def valid_move?(from_pile, to_pile)
    to_tower = self.towers[to_pile]
    from_tower = self.towers[from_pile]

    # no disk can be placed on top of a smaller disk
    if from_tower.last.nil?
      false
    elsif to_tower.last.nil? || from_tower.last < to_tower.last
      true
    else
      false
    end
  end

  def move(from_pile, to_pile)
    to_tower = self.towers[to_pile]
    from_tower = self.towers[from_pile]

    to_tower.push(from_tower.pop)
  end

  def display
    system('clear')
    puts self.render
  end

  def render
    'Tower 0: ' + self.towers[0].join(' ') + "\n" +
    'Tower 1: ' + self.towers[1].join(' ') + "\n" +
    'Tower 2: ' + self.towers[2].join(' ') + "\n"
  end

  def won?
    if self.towers[1].count == 3 || self.towers[2].count == 3
      true
    else
      false
    end
  end

end
